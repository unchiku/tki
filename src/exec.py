class exec():
    def __init__(self, py_source):
        self.source = py_source
    def exec(self):
        from contextlib import redirect_stdout
        import io
        f = io.StringIO()
        with redirect_stdout(f):
            exec(self.source)
        return f.getvalue()
