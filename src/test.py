if __name__ == '__main__':
    if input('Test until transform.py? (y/n) ').lower() == 'y':
        print('Testing until transform.py...')
        import split, translate, transform
        s = split.split(input('Input '))
        t = translate.translate(s.split())
        tr = transform.transform(t.translate())
        print('Output')
        print(tr.transform())
    if input('Test all? (Y/n) ').lower() == 'y':
        print('Testing all...')
        import split, translate, transform, exec
        s = split.split(input('Input '))
        t = translate.translate(s.split())
        tr = transform.transform(t.translate())
        e = exec.exec(tr.transform())
        print('Output')
        print(e.exec())
    else:
        print('Ending...')