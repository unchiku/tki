class transform():
    def __init__(self, colon_source):
        self.source = colon_source
    def transform(self):
        source2 = ''
        for i in range(len(self.source.split('\n'))):
            line = self.source.split('\n')[i]
            parts = line.split(':')
            parts.reverse()
            parts2 = ''
            for i in range(len(parts)):
                part = parts[i]
                parts2 += part
                if i != len(parts) - 1:
                    parts2 += '('
            parts2 += ')'*(len(parts)-1)
            source2 += parts2 + '\n'
        return source2

if __name__ == '__main__':
    import split, translate
    print('Input')
    s = split.split(input())
    t = translate.translate(s.split())
    tr = transform(t.translate())
    print('Output')
    print(tr.transform())