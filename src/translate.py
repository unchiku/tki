import dict

class translate():
    def __init__(self, data):
        self.data = data
        self.dict = dict.dict
    def _replace(self):
        out = ''
        skip = False
        for word in self.data:
            if word[0] == '「':
                skip = True
                out += self.dict[word[1]]
                continue
            elif word[0] == '」':
                skip = False
            if skip:
                out += word[0]
                continue
            if word[0] == 'EOS':
                continue
            out += self.dict[word[1]]
        return out
    def translate(self):
        return self._replace()

if __name__ == '__main__':
    import split
    s = split.split(input())
    t = translate(s.split())
    print(t.translate())

# Unchiku v1 (Tki v0) Diagram
#
# * WIP
# 「こんにちは」って書く。
# split
# [['「', '「', '「', '記号-括弧開', '', ''], ['こんにちは', 'コンニチハ', 'こんにちは', '感動詞', '', ''], ['」', '」', '」', '記号-括弧閉', '', ''], ['って', 'ッテ', 'って', '助詞-格助詞-連語', '', ''], ['書く', 'カク', '書く', '動詞-自立', '五段・カ行イ音便', '基本形'], ['。', '。', '。', '記号-句点', '', ''], ['EOS']]
# translate
# 'こんにちは':print
# transform
# print('こんにちは')
# exec *