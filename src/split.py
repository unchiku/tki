import MeCab
class split():
    def __init__(self, text):
        self.text = text
    def _split(self):
        chasen = MeCab.Tagger("-Ochasen")
        return chasen.parse(self.text)
    def _simplify(self, data):
        lines = data.split('\n')
        lines.pop(-1)
        for i in range(len(lines)):
            line = lines[i]
            lines[i] = line.split('\t')
        return lines
    def split(self):
        return self._simplify(self._split())

if __name__== '__main__':
    s = split(input())
    print(s.split())